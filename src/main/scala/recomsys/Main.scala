package recomsys

import java.io.{BufferedWriter, File, FileWriter}

import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer}
import org.apache.spark.ml.linalg.{SparseVector, Vector}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window
import net.liftweb.json.JsonDSL._
import net.liftweb.json.JsonAST._
import net.liftweb.json.JObject
import net.liftweb.json.JArray

object Main extends App {
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val recommendedIds = Seq(4041, 36, 787, 832, 1464, 2032)

  val spark: SparkSession = SparkSession.builder()
    .master("local[1]")
    .appName("RECOMMENDATION SYSTEM")
    .getOrCreate()

  val df: DataFrame = spark.read.json("src/main/resources/DO_record_per_line.json").toDF
  df.createOrReplaceGlobalTempView("df")
  df.printSchema()

  val df1: DataFrame = df.withColumn("desc", regexp_replace(df("desc"), "[^\\w+]+", " "))

  import spark.implicits._

  val tokenizer = new Tokenizer().setInputCol("desc").setOutputCol("words")
  val wordsData = tokenizer.transform(df1)
  val hashingTF = new HashingTF()
    .setInputCol("words").setOutputCol("rawFeatures").setNumFeatures(10000)

  val dataWithFeatures = hashingTF.transform(wordsData)
  val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
  val idfModel = idf.fit(dataWithFeatures)

  val transformedData: DataFrame = idfModel.transform(dataWithFeatures)
  transformedData.show(2)

  val dense = udf((v: Vector) => v.toDense)
  val newDf = transformedData
    .withColumn("dense_features", dense(transformedData("features")))

  val cosineSimilarity = udf { (x: Vector, y: Vector) =>
    val matrix1 = x.toArray
    val matrix2 = y.toArray
    val matrixRes1 = scala.math.sqrt(matrix1.map(x => x*x).sum)
    val matrixRes2 = scala.math.sqrt(matrix2.map(x => x*x).sum)
    val scalarSum = matrix1.zip(matrix2).map(p => p._1*p._2).sum
    scalarSum/(matrixRes1*matrixRes2)
  }

  val filteredDataFrame = newDf
    .filter(col("id").isin(recommendedIds: _*))
    .select(col("id").alias("id_recommended"),
      col("dense_features").alias("dense_frd"),
      col("lang").alias("lang_frd")
    )

  val joinedDf = newDf.join(
    broadcast(filteredDataFrame),
    col("id") =!= col("id_recommended") && col("lang") === col("lang_frd")
  ).withColumn(
    "cosine_sim",
    cosineSimilarity(col("dense_frd"), col("dense_features"))
  )

  val resultDataFrame = joinedDf
    .filter(col("lang")==="en")
    .withColumn("cosine_sim", when(col("cosine_sim").isNaN, 0).otherwise(col("cosine_sim")))
    .withColumn("rank", row_number().over(
      Window.partitionBy(col("id_recommended")).orderBy(col("cosine_sim").desc)))
    .filter(col("rank")between(2,11))

  resultDataFrame.show(10)
  val file = new File("result.json")
  val bw = new BufferedWriter(new FileWriter(file))

  bw.write("{")
  recommendedIds.foreach(i => {
    val recs = resultDataFrame.select("id").where(
      col("id_recommended") === i
    ).rdd.map(r => r(0).asInstanceOf[Long]).collect().toList

    val jArray: JArray = recs;
    bw.write("\"" + i + "\":" + prettyRender(jArray))
    if (i != recommendedIds.last) {
      bw.write(",\n")
    }
  })
  bw.write("}")
  bw.close()
  println("success write to file")

}
